
create table addresses (addressID integer not null, city varchar(255), country varchar(255), floor varchar(255), housenumber integer, postalCode integer, street varchar(255), primary key (addressID)) type=MyISAM
create table Users (userID integer not null, emailAddress varchar(255), firstName varchar(255), sureName varchar(255), addressID integer, primary key (userID)) type=MyISAM
create table Accounts (accountID integer not null, administrator bit, passwordsecure varchar(255), userID integer, primary key (accountID)) type=MyISAM
create table Books (ISBN bigint not null, Picture varchar(255), Summary varchar(255), title varchar(255), author integer, primary key (ISBN)) type=MyISAM
create table Orders (orderID integer not null auto_increment, orderDate date, userID integer, primary key (orderID)) type=MyISAM
create table stocks (StockID bigint not null, price double precision, Quantity integer, StockDate date, ISBN bigint, primary key (StockID)) type=MyISAM
create table order_book_link (linkID integer not null auto_increment, discount integer, quantity integer, ISBN bigint, orderID integer, primary key (linkID)) type=MyISAM
create table Carts (cartID integer not null auto_increment, cart integer, ISBN bigint, userID integer, primary key (cartID)) type=MyISAM

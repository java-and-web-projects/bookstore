
package be.samish.lookforbook.home;

import be.samish.lookforbook.book.Book;
import be.samish.lookforbook.book.BookRepo;
import be.samish.lookforbook.book.BookRepoImpl;
import be.samish.lookforbook.user.User;
import be.samish.lookforbook.cart.Cart;
import be.samish.lookforbook.cart.CartRepo;
import be.samish.lookforbook.cart.CartRepoImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(value = "/addbookto")
public class SaveBookServlet extends HttpServlet {
    List<Cart> cartList;
    List<Cart> favoriteList;
    BookRepo bookRepo;
    CartRepo cartRepo;

    @Override
    public void init() throws ServletException {
        cartList = new ArrayList<>();
        favoriteList = new ArrayList<>();
        bookRepo = new BookRepoImpl();
        cartRepo = new CartRepoImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        Book book = bookRepo.find(Long.parseLong(req.getParameter("isbn")));
        int quantity = Integer.parseInt(req.getParameter("add"));
        System.out.println(quantity);
        if (quantity < 0) {
            if (quantity == -1) cartList.removeIf(currentCart -> currentCart.getIsbn().equals(book));
            else favoriteList.removeIf(currentCart -> currentCart.getIsbn().equals(book));
        } else {
            User user = (User) session.getAttribute("User");
            Cart cart = new Cart(user, book, quantity);
            if (user != null) {
                cartRepo.save(cart);
            }
            if (quantity == 0) {
                favoriteList.removeIf(currentCart -> currentCart.getIsbn().equals(book));
                favoriteList.add(cart);
            } else {
                cartList.forEach(currentCart -> {
                    if (currentCart.getIsbn().equals(book)) {
                        currentCart.setCart(currentCart.getCart() + 1);
                        cart.setCart(0);
                    }
                });
                if (cart.getCart() >= 0) {
                    cartList.add(cart);
                }
            }
        }



        session.setAttribute("favoriteCount",favoriteList.size()==0?"":favoriteList.size());
        session.setAttribute("favoriteList", favoriteList);
        session.setAttribute("cartCount",cartList.size()==0?"":cartList.size());
        session.setAttribute("cartList", cartList);
        req.getRequestDispatcher("/index.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}

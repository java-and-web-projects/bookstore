
package be.samish.lookforbook.home;

import be.samish.lookforbook.book.Book;
import be.samish.lookforbook.book.BookRepo;
import be.samish.lookforbook.stock.Stock;
import be.samish.lookforbook.stock.StockRepo;
import be.samish.lookforbook.stock.StockRepoImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/booook.jsp")
public class HomeServlet extends HttpServlet {
    StockRepo repo;

    @Override
    public void init() throws ServletException {
        System.out.println("the init methode is running");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Stock stock = repo.find(Long.parseLong(req.getParameter("isbn")));
        System.out.println("get methode initiated");
        req.setAttribute("stock", stock);
        req.getRequestDispatcher("/index1.jsp");
        System.out.println("get ended ok");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}

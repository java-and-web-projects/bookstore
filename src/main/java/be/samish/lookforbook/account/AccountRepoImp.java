package be.samish.lookforbook.account;

import be.samish.lookforbook.handlers.EntityHandler;
import be.samish.lookforbook.handlers.EntityHandlerImpl;

public class AccountRepoImp implements AccountRepo{
    @Override
    public void save(Account account) {
        try (EntityHandler handler = new EntityHandlerImpl("lookforbook")) {
            handler.beginTransaction();

            handler.merge(account);

            handler.commitTransaction();
        }
    }

    @Override
    public Account find(int accountID) {
        Account account;

        try (EntityHandler handler = new EntityHandlerImpl("lookforbook")) {
            handler.beginTransaction();

            account = handler.find(Account.class, accountID);

            handler.commitTransaction();
        }


        return account;
    }
}

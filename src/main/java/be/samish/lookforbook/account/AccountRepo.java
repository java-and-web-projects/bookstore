package be.samish.lookforbook.account;

public interface AccountRepo {
    void save(Account account);

    Account find(int accountID);
}

package be.samish.lookforbook.account;

import be.samish.lookforbook.user.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "Accounts")
public class Account implements Serializable {
    @Id
    @Column(name = "accountID")
    private int accountId;

    @OneToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "userID")
    private User user;

    @Column(name = "passwordsecure")
    private String password;

    @Column(name = "administrator")
    private boolean administrator;

    public Account() {
    }

    public Account(User user, String password, boolean administrator) {
        this.user = user;
        this.password = password;
        this.administrator = administrator;
        this.accountId = user.getUserId();
    }


    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public boolean isAdministrator() {
        return administrator;
    }

    public void setAdministrator(boolean administrator) {
        this.administrator = administrator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return  getAccountId() == account.getAccountId() &&
                administrator == account.administrator &&
                Objects.equals(accountId, account.accountId) &&
                Objects.equals(password, account.password);
    }

    @Override
    public int hashCode() {
        return accountId;
    }
}

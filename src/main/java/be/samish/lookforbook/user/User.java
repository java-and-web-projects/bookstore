package be.samish.lookforbook.user;

import be.samish.lookforbook.address.Address;
import be.samish.lookforbook.order.Order;
import be.samish.lookforbook.cart.Cart;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Users")
public class User implements Serializable {
    @Id
    @Column(name = "userID")
    private int userId;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "addressID")
    private Address address;

    @Column(name = "firstName")
    private String name;

    @Column(name = "sureName")
    private String sureName;

    @Column(name = "emailAddress")
    private String email;

    @OneToMany(mappedBy = "user", cascade = CascadeType.MERGE)
    List<Cart> carts = new ArrayList<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.MERGE)
    List<Order> orders = new ArrayList<>();

    public User(){
    }

    public User(Address address, String name, String sureName, String email) {

        this.name = name;
        this.sureName = sureName;
        this.email = email;
        this.userId = hashCode();
        setAddress(address);
    }

    public void addCart(Cart cart) {
        carts.add(cart);
    }
    public void addOrder(Order order) {
        orders.add(order);
    }


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }


    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
        address.addUser(this);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getSureName() {
        return sureName;
    }

    public void setSureName(String sureName) {
        this.sureName = sureName;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return userId == user.userId &&
                address.getAddressId() == user.getAddress().getAddressId() &&
                Objects.equals(name, user.name) &&
                Objects.equals(sureName, user.sureName) &&
                Objects.equals(email, user.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email.toLowerCase().strip());
    }

    @Override
    public String toString() {
        return "User{" +
                "address=" + address +
                ", name='" + name + '\'' +
                ", sureName='" + sureName + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}

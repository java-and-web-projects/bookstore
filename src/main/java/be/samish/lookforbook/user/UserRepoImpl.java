package be.samish.lookforbook.user;

import be.samish.lookforbook.handlers.EntityHandler;
import be.samish.lookforbook.handlers.EntityHandlerImpl;

public class UserRepoImpl implements UserRepo{
    @Override
    public void save(User user) {
        try (EntityHandler handler = new EntityHandlerImpl("lookforbook")) {
            handler.beginTransaction();

            handler.merge(user);

            handler.commitTransaction();
        }
    }

    @Override
    public User find(int userID) {
        User user;

        try (EntityHandler handler = new EntityHandlerImpl("lookforbook")) {
            handler.beginTransaction();

            user = handler.find(User.class, userID);

            handler.commitTransaction();
        }


        return user;
    }
}

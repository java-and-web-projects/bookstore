package be.samish.lookforbook.user;


public interface UserRepo {
    void save(User user);

    User find(int userID);
}

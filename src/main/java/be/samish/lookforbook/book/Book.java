package be.samish.lookforbook.book;

import be.samish.lookforbook.orderbooklink.OrderBookLink;
import be.samish.lookforbook.user.User;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Books")
public class Book implements Serializable {

    @Id
    @Column(name = "ISBN")
    private long isbn;

    @Column(name = "title")
    private String title;

    @Column(name = "Picture")
    private String picture;

    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "author")
    private User author;

    @Column(name = "Summary")
    private String summary;

    @OneToMany(mappedBy = "isbn", fetch = FetchType.EAGER)
    List<OrderBookLink> orderLinkList = new ArrayList<>();

    public Book() {
    }

    public Book(long isbn, String title, String picture, User author, String summary) {
        this.isbn = isbn;
        this.title = title;
        this.picture = picture;
        this.author = author;
        setSummary(summary);
    }

    public void addToOrderLink(OrderBookLink orderBookLink) {
        orderLinkList.add(orderBookLink);
    }

    public long getIsbn() {
        return isbn;
    }

    public void setIsbn(long isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }


    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }


    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        if (summary.length() > 500) {
            int pointAfter500 = summary.indexOf('.', 500);
            summary = summary.substring(0,pointAfter500>0&&pointAfter500<600?pointAfter500:550);
        }
        this.summary = summary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(isbn, book.isbn) &&
                Objects.equals(title, book.title);

    }

    @Override
    public int hashCode() {
        int result = Objects.hash(isbn, title, author, summary);
        return result;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", picture='" + picture + '\'' +
                ", author=" + author +
                '}';
    }
}

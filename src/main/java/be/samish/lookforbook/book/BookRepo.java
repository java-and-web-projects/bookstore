package be.samish.lookforbook.book;


public interface BookRepo {
    void save(Book book);

    Book find(long ISBN);
}

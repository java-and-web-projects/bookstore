package be.samish.lookforbook.book;

import be.samish.lookforbook.handlers.EntityHandler;
import be.samish.lookforbook.handlers.EntityHandlerImpl;

public class BookRepoImpl implements BookRepo{
    @Override
    public void save(Book book) {
        try (EntityHandler handler = new EntityHandlerImpl("lookforbook")) {
            handler.beginTransaction();

            handler.merge(book);

            handler.commitTransaction();
        }
    }

    @Override
    public Book find(long ISBN) {
        Book book;

        try (EntityHandler handler = new EntityHandlerImpl("lookforbook")) {
            handler.beginTransaction();

            book = handler.find(Book.class, ISBN);

            handler.commitTransaction();
        }


        return book;
    }
}

package be.samish.lookforbook.stock;

import be.samish.lookforbook.handlers.EntityHandler;
import be.samish.lookforbook.handlers.EntityHandlerImpl;

import java.util.ArrayList;
import java.util.List;

public class StockRepoImpl implements StockRepo{
    @Override
    public void save(Stock stock) {
        try (EntityHandler handler = new EntityHandlerImpl("lookforbook")) {
            handler.beginTransaction();

            handler.merge(stock);

            handler.commitTransaction();
        }
    }

    @Override
    public Stock find(long stockID) {
        Stock stock;

        try (EntityHandler handler = new EntityHandlerImpl("lookforbook")) {
            handler.beginTransaction();

            stock = handler.find(Stock.class, stockID);

            handler.commitTransaction();
        }


        return stock;
    }

    @Override
    public List<Stock> getPopularStock() {
        List<Stock> stocks = new ArrayList<>();
        for (int i = 1; i < 5; i++) {
            stocks.add(find(i));
        }
        return stocks;
    }

}

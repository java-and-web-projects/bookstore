package be.samish.lookforbook.stock;

import be.samish.lookforbook.book.Book;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "stocks")
public class Stock implements Serializable {

    @Id
    @Column(name = "StockID")
    private long stockId;

    @Column(name = "StockDate")
    private LocalDate stockDate;

    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "ISBN")
    private Book isbn;

    @Column(name = "price")
    private double price;

    @Column(name = "Quantity")
    private int quantity;

    public Stock() {
    }

    public Stock(LocalDate stockDate, Book isbn, double price, int quantity) {
        this.stockDate = stockDate;
        this.isbn = isbn;
        this.price = price;
        this.quantity = quantity;
        this.stockId = isbn.getIsbn();
    }

    public long getStockId() {
        return stockId;
    }

    public void setStockId(long stockId) {
        this.stockId = stockId;
    }


    public LocalDate getStockDate() {
        return stockDate;
    }

    public void setStockDate(LocalDate stockDate) {
        this.stockDate = stockDate;
    }


    public Book getIsbn() {
        return isbn;
    }

    public void setIsbn(Book isbn) {
        this.isbn = isbn;
    }


    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stock stock = (Stock) o;
        return stockId == stock.stockId &&
                Double.compare(stock.price, price) == 0 &&
                quantity == stock.quantity &&
                Objects.equals(stockDate, stock.stockDate) &&
                Objects.equals(isbn, stock.isbn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stockId, stockDate, isbn, price, quantity);
    }

    @Override
    public String toString() {
        return "Stock{" +
                "stockId=" + stockId +
                ", stockDate=" + stockDate +
                ", isbn=" + isbn +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}

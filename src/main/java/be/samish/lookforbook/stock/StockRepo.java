package be.samish.lookforbook.stock;

import java.util.List;

public interface StockRepo {
    void save(Stock stock);

    Stock find(long stockID);

    List<Stock> getPopularStock();

}

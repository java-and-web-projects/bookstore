package be.samish.lookforbook;

import be.samish.lookforbook.address.Address;
import be.samish.lookforbook.book.Book;
import be.samish.lookforbook.book.BookRepo;
import be.samish.lookforbook.book.BookRepoImpl;
import be.samish.lookforbook.stock.Stock;
import be.samish.lookforbook.stock.StockRepo;
import be.samish.lookforbook.stock.StockRepoImpl;
import be.samish.lookforbook.user.User;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) {

        Address address = new Address("", 0, "", 0, "GuildFord", "England");
        User user = new User(address, "Lewis", "Carroll", "LewisCarrol@");
        //AddressRepo addressRepo = new AddressRepoImpl();
        //addressRepo.save(address);

        //AccountRepo accountRepo = new AccountRepoImp();
        //Account account = new Account(user, "securepass", true);
        //accountRepo.save(account);
        //9780721416540L

        Book book = new Book(1, "Alice in Wonderland", "https://pictures.abebooks.com/isbn/9780721416540-es.jpg", user, "Alice's Adventures in Wonderland is an 1865 novel written by English author Charles Lutwidge Dodgson under the pseudonym Lewis Carroll. It tells of a young girl named Alice falling through a rabbit hole into a fantasy world populated by peculiar, anthropomorphic creatures.");
        BookRepo bookRepo = new BookRepoImpl();
        bookRepo.save(book);
        Stock stock = new Stock(LocalDate.now(), book, 15.00, 5);
        StockRepo stockRepo = new StockRepoImpl();
        stockRepo.save(stock);
        address = new Address("",0,"",0,"Paris","France");
        user = new User(address,"Octacve","Mirbeau","OctaveMirbeau@");
        //9782070378999L
        book = new Book(2,"Le Jardin des supplices","http://ecx.images-amazon.com/images/I/41NY8HS858L._SL160_.jpg",user,"Quelle plume que celle de M. Mirbeau !\n" +
                "C'est avec l'apanage d'un critique d'art que l'auteur décrit les souffrances des suppliciés, d'une obscénité absolue, dans un pays où les bourreaux se disent artistes incompris et où les immondices côtoient les parfums les plus doux et les végétations luxuriantes. Par moment, l'on se rappelle la plume du Huysmans d' \"A rebours\" ou du Barbey d'Aurevilly des \"Diaboliques\".\n" +
                "Probablement un des ouvrages qui symbolise au mieux le décadentisme, avec les deux ouvrages sus-cités et les contes cruels de Villiers de l'Isle.\n" +
                "Le personnage principal, sans scrupule aucun pour la droiture et la morale, se voit banni d'une administration elle-même vérolée et corrompue. Il est envoyé comme botaniste (sans qu’il n’y connaisse mot en la matière) en Chine, pays alors chargé de mystères et destination privilégiée de maints aristocrates neurasthéniques. Il y rencontre une dame dont les moeurs sont comparables à celles d'un(e) démon succube, dévorée par une sinistre passion de la déliquescence des choses et des êtres.");
        stock = new Stock(LocalDate.now(), book, 10.00, 5);
        stockRepo.save(stock);
        address = new Address("", 0, "", 0, "GuildFord", "England");
        user = new User(address,"Arthur","Doyle","ArthurDoyle@");
        //9789385492839L
        book = new Book(3,"The Adventures Sherlock Holmes","https://d332fetfsfsfty.cloudfront.net/books_image/9789385492839.jpg",user,"" +
                "Sherlock Holmes, the very name immediately conjures up an image of a tall, thin, shrewd man dressed in a loose overcoat, smoking a pipe and sporting a deerstalker hat. The famous literary detective's keen deduction skills and powers of observation are legendary, and The Adventures of Sherlock Holmes is where it all began.“A Scandal in Bohemia” was first published in the Strand Magazine in 1891, and since then, Doyle and his stories have collectively captured the world's attention and entranced readers endlessly.J");
        stock = new Stock(LocalDate.now(), book, 20.00, 2);
        stockRepo.save(stock);
        address = new Address("", 0, "", 0, "Athene", "Greece");
        user = new User(address,"Plato","Plato","Plato@");
        //9781484170977L
        book = new Book(4, "ion", "https://s.s-bol.com/imgbase0/imagebase3/large/FC/4/7/7/9/9200000022909774.jpg", user, "" +
                "ON By Plato The Ion is the shortest, or nearly the shortest, of all the writings which bear the name of Plato, and is not authenticated by any early external testimony. The grace and beauty of this little work supply the only, and perhaps a sufficient, proof of its genuineness. The plan is simple; the dramatic interest consists entirely in the contrast between the irony of Socrates and the transparent vanity and childlike enthusiasm of the rhapsode Ion. The theme of the Dialogue may possibly have been suggested by the passage of Xenophon's Memorabilia in which the rhapsodists are described by Euthydemus as 'very precise about the exact words of Homer, but very idiotic themselves");
        stock = new Stock(LocalDate.now(), book, 20.00, 1);
        stockRepo.save(stock);


/*        Order order = new Order(LocalDate.now(), user);
        OrderBookLink orderBookLink = new OrderBookLink(order, book, 5, 0);
        OrderBookLinkRepo orderBookLinkRepo = new OrderBookLinkRepoImpl();
        orderBookLinkRepo.save(orderBookLink);

        Wish wish = new Wish(user, book);
        WishRepo wishRepo = new WishRepoImpl();
        wishRepo.save(wish);


        StockRepo repo = new StockRepoImpl();
        List<Stock> stocks = new ArrayList<>();
        for (int i = 1; i < 5; i++) {
            stocks.add(repo.find(i));
        }
        System.out.println(stocks);
*/
    }


}

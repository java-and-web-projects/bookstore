package be.samish.lookforbook.orderbooklink;

import be.samish.lookforbook.handlers.EntityHandler;
import be.samish.lookforbook.handlers.EntityHandlerImpl;

public class OrderBookLinkRepoImpl implements OrderBookLinkRepo{
    @Override
    public void save(OrderBookLink orderBookLink) {
        try (EntityHandler handler = new EntityHandlerImpl("lookforbook")) {
            handler.beginTransaction();

            handler.merge(orderBookLink);

            handler.commitTransaction();
        }
    }

    @Override
    public OrderBookLink find(int orderLinkID) {
        OrderBookLink orderBookLink;

        try (EntityHandler handler = new EntityHandlerImpl("lookforbook")) {
            handler.beginTransaction();

            orderBookLink = handler.find(OrderBookLink.class, orderLinkID);

            handler.commitTransaction();
        }


        return orderBookLink;
    }
}

package be.samish.lookforbook.orderbooklink;

import be.samish.lookforbook.book.Book;
import be.samish.lookforbook.order.Order;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "order_book_link")
public class OrderBookLink implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "linkID")
    private int linkId;

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "orderID")
    private Order order;

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "ISBN")
    private Book isbn;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "discount")
    private int discount;

    public OrderBookLink() {
    }

    public OrderBookLink(Order order, Book book, int quantity, int discount) {
        this.discount = discount;
        this.quantity = quantity;
        setOrder(order);
        setIsbn(book);
    }


    public int getLinkId() {
        return linkId;
    }

    public void setLinkId(int linkId) {
        this.linkId = linkId;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        order.addBooks(this);
        this.order = order;
    }

    public Book getIsbn() {
        return isbn;
    }

    public void setIsbn(Book isbn) {
        isbn.addToOrderLink(this);
        this.isbn = isbn;
    }


    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderBookLink that = (OrderBookLink) o;
        return linkId == that.linkId &&
                order == that.order &&
                quantity == that.quantity &&
                discount == that.discount &&
                Objects.equals(isbn, that.isbn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(linkId, order, isbn, quantity, discount);
    }
}

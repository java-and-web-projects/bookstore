package be.samish.lookforbook.orderbooklink;

public interface OrderBookLinkRepo {
    void save(OrderBookLink orderBookLink);

    OrderBookLink find(int orderlinkID);
}

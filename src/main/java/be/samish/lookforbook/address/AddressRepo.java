package be.samish.lookforbook.address;

public interface AddressRepo {
    void save(Address address);

    Address find(int addressId);
}

package be.samish.lookforbook.address;


import be.samish.lookforbook.handlers.EntityHandler;
import be.samish.lookforbook.handlers.EntityHandlerImpl;

public class AddressRepoImpl implements AddressRepo {

    @Override
    public void save(Address address) {
        try (EntityHandler handler = new EntityHandlerImpl("lookforbook")) {
            handler.beginTransaction();

            handler.merge(address);

            handler.commitTransaction();
        }
    }

    @Override
    public Address find(int addressID) {
        Address address;

        try (EntityHandler handler = new EntityHandlerImpl("lookforbook")) {
            handler.beginTransaction();

            address = handler.find(Address.class, addressID);

            handler.commitTransaction();
        }


        return address;
    }
}

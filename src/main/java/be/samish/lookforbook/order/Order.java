package be.samish.lookforbook.order;

import be.samish.lookforbook.orderbooklink.OrderBookLink;
import be.samish.lookforbook.user.User;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Orders")
public class Order implements Serializable {

    @Id
    @Column(name = "orderID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int orderId;

    @Column(name = "orderDate")
    private LocalDate orderDate;

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "userID")
    private User user;

    @OneToMany(mappedBy = "order", fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    List<OrderBookLink> booksList = new ArrayList<>();

    public Order(LocalDate orderDate, User user) {
        this.orderDate = orderDate;
        setUser(user);
    }

    public Order() {
    }

    public void addBooks(OrderBookLink orderBookLink) {
        booksList.add(orderBookLink);
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }


    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        user.addOrder(this);
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return orderId == order.orderId &&
                user == order.user &&
                Objects.equals(orderDate, order.orderDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, orderDate, user);
    }
}

package be.samish.lookforbook.order;

import be.samish.lookforbook.handlers.EntityHandler;
import be.samish.lookforbook.handlers.EntityHandlerImpl;

public class OrderRepoImpl implements OrderRepo {
    @Override
    public void save(Order order) {
        try (EntityHandler handler = new EntityHandlerImpl("lookforbook")) {
            handler.beginTransaction();

            handler.merge(order);

            handler.commitTransaction();
        }
    }

    @Override
    public Order find(int orderID) {
        Order order;

        try (EntityHandler handler = new EntityHandlerImpl("lookforbook")) {
            handler.beginTransaction();

            order = handler.find(Order.class, orderID);

            handler.commitTransaction();
        }


        return order;
    }
}

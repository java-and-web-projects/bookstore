package be.samish.lookforbook.order;


public interface OrderRepo {
    void save(Order order);

    Order find(int orderID);
}

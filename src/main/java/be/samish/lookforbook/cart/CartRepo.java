package be.samish.lookforbook.cart;

public interface CartRepo {
    void save(Cart cart);

    Cart find(int wishListID);
}

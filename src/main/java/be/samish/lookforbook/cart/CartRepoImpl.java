package be.samish.lookforbook.cart;

import be.samish.lookforbook.handlers.EntityHandler;
import be.samish.lookforbook.handlers.EntityHandlerImpl;

public class CartRepoImpl implements CartRepo {
    @Override
    public void save(Cart cart) {
        try (EntityHandler handler = new EntityHandlerImpl("lookforbook")) {
            handler.beginTransaction();

            handler.merge(cart);

            handler.commitTransaction();
        }
    }

    @Override
    public Cart find(int wishListID) {
        Cart cart;

        try (EntityHandler handler = new EntityHandlerImpl("lookforbook")) {
            handler.beginTransaction();

            cart = handler.find(Cart.class, wishListID);

            handler.commitTransaction();
        }


        return cart;
    }
}

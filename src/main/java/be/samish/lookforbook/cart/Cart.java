package be.samish.lookforbook.cart;

import be.samish.lookforbook.book.Book;
import be.samish.lookforbook.user.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "Carts")
public class Cart implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cartID")
    private int cartID;

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "userID")
    private User user;

    @OneToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "ISBN")
    private Book isbn;


    public int getCart() {
        return cart;
    }

    public void setCart(int cart) {
        this.cart = cart;
    }

    @Column(name = "cart")
    private int cart;

    public Cart() {
    }

    public Cart(User user, Book isbn, int cart) {
        setUser(user);
        this.isbn = isbn;
        this.cart = cart;
    }

    public int getCartId() {
        return cartID;
    }

    public void setCartId(int cartId) {
        this.cartID = cartId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        if(user == null) return;
        user.addCart(this);
        this.user = user;
    }

    public Book getIsbn() {
        return isbn;
    }

    public void setIsbn(Book isbn) {
        this.isbn = isbn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cart cart = (Cart) o;
        return cartID == cart.cartID &&
                user == cart.user &&
                Objects.equals(isbn, cart.isbn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cartID, user, isbn);
    }
}

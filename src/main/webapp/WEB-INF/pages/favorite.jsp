<%@ page import="java.util.List" %>
<%@ page import="be.samish.lookforbook.cart.Cart" %>
<%@ page import="be.samish.lookforbook.stock.StockRepoImpl" %>
<%@ page import="be.samish.lookforbook.stock.StockRepo" %>
<%@ page import="be.samish.lookforbook.book.Book" %>
<%@ page import="be.samish.lookforbook.stock.Stock" %><%--
  Created by IntelliJ IDEA.
  User: Samir
  Date: 19/06/2020
  Time: 01:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="favoriteMenu cart-bg-overlay"></div>
<div class="favoriteMenu right-side-cart-area">
    <%  List<Cart> favoriteList = (List<Cart>)session.getAttribute("favoriteList");
        StockRepo stockRepo = new StockRepoImpl();
        Object favoriteCount = session.getAttribute("favoriteCount");%>
    <!-- Cart Button -->
    <div class="cart-button">
        <a href="#" class="rightSideCart"><img src="resources/img/core-img/heart.svg" alt=""> <span><%=favoriteCount==null?"":favoriteCount%></span></a>
    </div>
    <div class="cart-content d-flex">
        <%
            if (favoriteList == null || favoriteList.isEmpty()) {%>

        <div class="cart-amount-summary">

            <h2>FavoriteList is empty, add some books!</h2>
        </div>
        <% }else {
        %>       <!-- Cart List Area -->
        <div class="cart-list"> <%
            for (Cart cart: favoriteList
            ) {
                Book book = cart.getIsbn();
                Stock stock = stockRepo.find(book.getIsbn());
        %>


            <!-- Single Cart Item -->
            <div class="single-cart-item">
                <a href="#" class="product-image">
                    <img src="<%=book.getPicture()%>" class="cart-thumb" alt="">
                    <!-- Cart Item Desc -->
                    <div class="cart-item-desc">
                        <span class="product-remove" href="addbookto?add=2-&isbn=<%=stock.getIsbn().getIsbn()%>"><i class="fa fa-close" aria-hidden="true"></i></span>
                        <span class="badge"><%=book.getAuthor().getName() + " "+book.getAuthor().getSureName()%></span>
                        <h6><%=book.getTitle()%></h6>
                        <p class="color">Quantity: <%=cart.getCart()%></p>
                        <p class="price"><%=stock.getPrice()%></p>
                    </div>
                </a>
            </div>

            <% } %>
        </div>
        <!-- Cart Summary -->
        <%}%>
    </div>
</div>
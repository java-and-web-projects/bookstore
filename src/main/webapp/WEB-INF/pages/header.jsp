<%--
  Created by IntelliJ IDEA.
  User: Samir
  Date: 19/06/2020
  Time: 23:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<header class="header_area">
    <div class="classy-nav-container breakpoint-off d-flex align-items-center justify-content-between bg-white">
        <!-- Classy Menu -->
        <nav class="classy-navbar" id="essenceNav">
            <!-- Logo -->
            <a class="nav-brand" href="./index.jsp"><img src="resources/img/core-img/logo.png" alt=""></a>
            <!-- Navbar Toggler -->
        </nav>

        <!-- Header Meta Data -->
        <div class="header-meta d-flex clearfix justify-content-end">
            <!-- Search Area -->
            <div class="search-area">
                <form action="#" method="post">
                    <input type="search" name="search" id="headerSearch" placeholder="Type for search">
                    <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                </form>
            </div>
            <!-- Favourite Area -->
            <div class="favourite-area">
                <a href="#" class="favoriteMenu booksMenuBtn"><img src="resources/img/core-img/heart.svg" alt=""><span><%Object favoriteCount = session.getAttribute("favoriteCount");%><%=favoriteCount==null?"":favoriteCount%></span></a></a>
            </div>
            <!-- User Login Info -->
            <div class="user-login-info">
                <a href="#" class="userMenu booksMenuBtn"><img src="resources/img/core-img/user.svg" alt=""></a>
            </div>
            <!-- Cart Area -->
            <div class="cart-area">
                <a href="#" class="cartMenu booksMenuBtn"><img src="resources/img/core-img/bag.svg" alt=""> <span><%Object cartCount = session.getAttribute("cartCount");%><%=cartCount==null?"":cartCount%></span></a>
            </div>
        </div>

    </div>
</header>

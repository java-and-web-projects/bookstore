<%@ page import="java.util.List" %>
<%@ page import="be.samish.lookforbook.cart.Cart" %>
<%@ page import="be.samish.lookforbook.stock.StockRepoImpl" %>
<%@ page import="be.samish.lookforbook.stock.StockRepo" %>
<%@ page import="be.samish.lookforbook.book.Book" %>
<%@ page import="be.samish.lookforbook.stock.Stock" %><%--
  Created by IntelliJ IDEA.
  User: Samir
  Date: 19/06/2020
  Time: 01:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="cartMenu cart-bg-overlay"></div>
<div class="cartMenu right-side-cart-area">
    <%  List<Cart> cartList = (List<Cart>)session.getAttribute("cartList");
        double total = 0;
        StockRepo stockRepo = new StockRepoImpl();
        Object cartCount = session.getAttribute("cartCount");%>
    <!-- Cart Button -->
    <div class="cart-button">
        <a href="#" class="rightSideCart"><img src="resources/img/core-img/bag.svg" alt=""> <span><%=cartCount==null?"":cartCount%></span></a>
    </div>
    <div class="cart-content d-flex">
<%
    if (cartList == null || cartList.isEmpty()) {%>

        <div class="cart-amount-summary">
            <h2>Cart is empty, add some books!</h2>
        </div>
        <% }else {
        %>       <!-- Cart List Area -->
        <div class="cart-list"> <%
            for (Cart cart: cartList
                 ) {
                Book book = cart.getIsbn();
                Stock stock = stockRepo.find(book.getIsbn());
                total += stock.getPrice();
        %>


            <!-- Single Cart Item -->
            <div class="single-cart-item">
                <a href="#" class="product-image">
                    <img src="<%=book.getPicture()%>" class="cart-thumb" alt="">
                    <!-- Cart Item Desc -->
                    <div class="cart-item-desc">
                        <span class="product-remove" href="addbookto?add=1-&isbn=<%=stock.getIsbn().getIsbn()%>"><i class="fa fa-close" aria-hidden="true"></i></span>
                        <span class="badge"><%=book.getAuthor().getName() + " "+book.getAuthor().getSureName()%></span>
                        <h6><%=book.getTitle()%></h6>
                        <p class="color">Quantity: <%=cart.getCart()%></p>
                        <p class="price"><%=stock.getPrice()%></p>
                    </div>
                </a>
            </div>

        <% } %>
        </div>
        <!-- Cart Summary -->
        <div class="cart-amount-summary">

            <h2>Summary</h2>
            <ul class="summary-table">
                <li><span>subtotal:</span> <span><%=total%></span></li>
                <li><span>delivery:</span> <span>Free</span></li>
                <li><span>discount:</span> <span>-15%</span></li>
                <li><span>total:</span> <span>€<%=total*0.85%></span></li>
            </ul>
            <div class="checkout-btn mt-100">
                <a href="checkout.html" class="btn essence-btn">check out</a>
            </div>
        </div>
            <%}%>
    </div>
</div>
<%@ page import="be.samish.lookforbook.stock.StockRepo" %>
<%@ page import="be.samish.lookforbook.stock.StockRepoImpl" %>
<%@ page import="java.util.List" %>
<%@ page import="be.samish.lookforbook.stock.Stock" %><%--
  Created by IntelliJ IDEA.
  User: Samir
  Date: 19/06/2020
  Time: 01:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<section class="new_arrivals_area section-padding-80 clearfix">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-heading text-center">
                    <h2>Popular Books</h2>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="popular-products-slides owl-carousel">
                    <% System.out.println("hello starting popular product ");
                        List<Stock> stocks;
                        StockRepo stockRepo = new StockRepoImpl();
                        stocks = stockRepo.getPopularStock();
                        System.out.println(stocks);
                        for (Stock stock : stocks
                        ) {

                        %>
                        <div class="single-product-wrapper">
                            <!-- Product Image -->
                            <div class="product-img">
                                <img src="<%=stock.getIsbn().getPicture()%>" alt="">
                                <!-- Favourite -->
                                <div class="product-favourite">
                                    <a href="addbookto?add=0&isbn=<%=stock.getIsbn().getIsbn()%>" class="favme fa fa-heart"></a>
                                </div>
                            </div>
                            <!-- Product Description -->
                            <div class="product-description">
                                <span><%=stock.getIsbn().getAuthor().getName() + " "+stock.getIsbn().getAuthor().getSureName()%></span>
                                <a href="bookpage.jsp?isbn=<%=stock.getIsbn().getIsbn()%>">
                                    <h6><%=stock.getIsbn().getTitle()%></h6>
                                </a>
                                <p class="product-price"><%=stock.getPrice()%></p>

                                <!-- Hover Content -->
                                <div class="hover-content">
                                    <!-- Add to Cart -->
                                    <div class="add-to-cart-btn">
                                        <a href="addbookto?add=1&isbn=<%=stock.getIsbn().getIsbn()%>" class="btn essence-btn">Add to Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <% }%>
                </div>
            </div>
        </div>
    </div>
</section>

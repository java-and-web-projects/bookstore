<%@ page import="be.samish.lookforbook.stock.Stock" %>
<%@ page import="be.samish.lookforbook.stock.StockRepo" %>
<%@ page import="be.samish.lookforbook.stock.StockRepoImpl" %><%--
  Created by IntelliJ IDEA.
  User: Samir
  Date: 19/06/2020
  Time: 23:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- ##### Single Product Details Area Start ##### -->
<section class="single_product_details_area d-flex align-items-center">
    <% StockRepo repo = new StockRepoImpl();
    Stock stock = repo.find(Long.parseLong(request.getParameter("isbn")));
    int available =stock.getQuantity();
    available = available/4 > 0 ? 100:25*available;%>
    <!-- Single Product Thumb -->
    <div class="single_product_thumb hei clearfix">
        <div class="text-center mt-15 mb-15">
            <img style="max-height: 500px;max-width: 450px;min-height:400px" src="<%=stock.getIsbn().getPicture()%>" alt="">
        </div>
    </div>

    <!-- Single Product Description -->
    <div class="single_product_desc clearfix">
        <span><%=stock.getIsbn().getAuthor().getName() + " "+stock.getIsbn().getAuthor().getSureName()%></span>
        <a href="cart.html">
            <h2><%=stock.getIsbn().getTitle()%></h2>
        </a>
        <p class="product-price">€ <%=stock.getPrice()%></p>
        <p class="product-desc"><%=stock.getIsbn().getSummary()%></p>

        <!-- Form -->
        <div class="progress mt-30 " style="width: 220px;;">
            <div class="progress-bar bg-success" style="width: <%=available%>%" aria-valuenow="25" aria-valuemin="0"
                 aria-valuemax="100">stock</div>
        </div>
        <form class="cart-form clearfix" method="post">
            <!-- Select Box -->
            <div class="select-box d-flex mt-15 mb-15">

                <select name="select" id="productColor">
                    <%= available>0?"<option value='value'>one book</option>":""%>
                    <%= available>25?"<option value='value'>two books</option>":""%>
                    <%= available>50?"<option value='value'>three book</option>":""%>
                </select>
            </div>

            <!-- Cart & Favourite Box -->
            <div class="cart-fav-box d-flex align-items-center">
                <!-- Cart -->
                <button type="submit" name="addtocart" value="5" class="btn essence-btn">Add to cart</button>
                <!-- Favourite -->
                <div class="product-favourite ml-4">
                    <a href="#" class="favme fa fa-heart"></a>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- ##### Single Product Details Area End ##### -->

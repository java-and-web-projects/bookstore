<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Look for Book</title>

    <!-- Favicon  -->
    <link rel="icon" href="resources/img/core-img/favicon.ico">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="resources/css/core-style.css">
    <link rel="stylesheet" href="resources/style.css">

</head>

<body>
    <!-- ##### Header Area Start ##### -->
    <!-- ##### Header Area Start ##### -->
    <jsp:include page="WEB-INF/pages/header.jsp" />
    <!-- ##### Header Area End ##### -->

    <!-- ##### Right Side Cart Area ##### -->
    <jsp:include page="WEB-INF/pages/cart.jsp" />
    <!-- ##### Right Side Cart End ##### -->
    <jsp:include page="WEB-INF/pages/product.jsp" />
    <!-- ##### Footer Area Start ##### -->
    <jsp:include page="WEB-INF/pages/footer.jsp" />
    <!-- ##### Footer Area End ##### -->

    <!-- jQuery (Necessary for All JavaScript Plugins) -->
    <jsp:include page="WEB-INF/pages/scripts.jsp" />

</body>

</html>